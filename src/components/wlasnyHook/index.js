import React from "react";
import useInput from "./customHook";
import  useArray from './listItem'


export default function TodoApp() {
  const [todo,setTodo, reset] = useInput("")
  const [date,setDate, resetDate] = useInput("");
  const todos = useArray([]);

  const onSubmit = e => {
    e.preventDefault();
    if (todo === ""  || !date) return{}
    todos.addItem(todo,date);
    reset()
    resetDate()
  };
  

  return (
    <div className="container" >
      <form  onSubmit={onSubmit}>
        <h1><label htmlFor="todo">Todo List</label></h1>
        <br />
        
        <input
          id="todo"
          type="text"
          onChange={setTodo}
          value={todo}
        />
        <input
          id="todoDate"
          type="date"
          value={date}
          onChange={setDate}
        />
        <button style={{backgroundColor:"#f0e5d8", border:"1px solid #ffc478", borderRadius:"5px", minHeight:"20px"}} type="submit">
          Add
        </button>
      </form>
      <div > 
        <table style={{border:'2px solid #ffc478', minWidth:'360px'}} >
          <tr >
            <th style={{border:'2px solid #bbdfc8'}}> Id </th>
            <th style={{border:'2px solid #bbdfc8'}}> Todo </th>
            <th style={{border:'2px solid #bbdfc8'}}> Deadline </th>
            <th style={{border:'2px solid #bbdfc8'}}> Done </th>
            <th style={{border:'2px solid #bbdfc8'}}> Delete </th>
            </tr>
          {todos.list.map(oneTodo => (
            <tr key={oneTodo.id} >
              <td  style={{backgroundColor: oneTodo.completed ? '#75cfb8' : null }}
              >
              {oneTodo.id+" "}   
            </td>
              <td  style={{backgroundColor: oneTodo.completed ? '#75cfb8' : null }}>
                {oneTodo.text+" "} 
              </td>
              <td  style={{backgroundColor: oneTodo.completed ? '#75cfb8' : null }}>
              {oneTodo.date+" "}
              </td>
              <td  style={{backgroundColor: oneTodo.completed ? '#75cfb8' : null }}>
              <input type='checkbox'
                onClick={() => todos.toggleItem(oneTodo.id)}>
              </input>
              </td>
              <td  style={{backgroundColor: oneTodo.completed ? '#75cfb8' : null }}>
              <button
                onClick={() => todos.removeItem(oneTodo.id)}
              >
                x
              </button>
              </td>
            </tr>
          ))}
        </table>
      </div>
    </div>
  );
}